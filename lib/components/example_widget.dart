import 'package:flutter/material.dart';

class ExampleWidget extends StatelessWidget {
  const ExampleWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(5),
        height: 100,
        width: 300,
        color: Colors.blue,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 80,
              child: Container(
                color: Colors.purple,
              ),
            ),
            Expanded(
              child: Container(
                width: 70,
                height: double.infinity,
                color: Colors.red,
                child: FittedBox(
                  child: Text(
                    'This is some long text for example card description',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
